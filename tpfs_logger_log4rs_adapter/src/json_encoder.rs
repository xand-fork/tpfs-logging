#[cfg(test)]
use chrono::NaiveDateTime;
use chrono::{DateTime, Utc};
use log::Record;
use log4rs::encode::{Encode, Write};
use serde::Serialize;
use std::{error::Error, thread};
use tpfs_logger_port::JsonEncodedLogMsg;

/// This is duplicative of the `JsonEncoder` built into Log4rs, but that didn't have many options
/// to configure what was logged.
#[derive(Debug)]
pub struct JsonEncoder;

impl Encode for JsonEncoder {
    fn encode(
        &self,
        w: &mut dyn Write,
        record: &Record<'_>,
    ) -> Result<(), Box<dyn Error + Sync + Send>> {
        let thread = thread::current();
        let message = JsonEncodedLogMsg {
            time: now(),
            msg: record.args(),
            level: record.level(),
            module_path: record.module_path(),
            file: record.file(),
            line: record.line(),
            target: record.target(),
            thread: thread.name(),
        };
        message.serialize(&mut serde_json::Serializer::new(&mut *w))?;
        w.write_all(NEWLINE.as_bytes())?;
        Ok(())
    }
}

#[allow(dead_code)]
#[cfg(windows)]
const NEWLINE: &str = "\r\n";
#[allow(dead_code)]
#[cfg(not(windows))]
const NEWLINE: &str = "\n";

// Simple way to get a consistent timestamp during tests.

#[allow(dead_code)]
#[cfg(not(test))]
fn now() -> DateTime<Utc> {
    Utc::now()
}

#[allow(dead_code)]
#[cfg(test)]
fn now() -> DateTime<Utc> {
    DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(0, 0), Utc)
}
