#![warn(clippy::all, clippy::nursery, clippy::pedantic, rust_2018_idioms)]
#![allow(bare_trait_objects)]
#![allow(clippy::match_bool)]
// To use the `unsafe` keyword, change to `#![allow(unsafe_code)]` (do not remove); aids auditing.
#![allow(unsafe_code)]
// Safety-critical application lints
#![deny(
    clippy::pedantic,
    clippy::float_cmp_const,
    clippy::indexing_slicing,
    clippy::integer_arithmetic,
    clippy::option_unwrap_used,
    clippy::result_unwrap_used
)]

// Uncomment before ship to reconcile use of possibly redundant crates, debug remnants, missing license files and more
//#![warn(clippy::cargo, clippy::restriction, missing_docs, warnings)]
//#![deny(warnings)]

mod json_encoder;
mod pattern_encoder_with_pid;

use crate::pattern_encoder_with_pid::PatternEncoderWithPid;
use json_encoder::JsonEncoder;
use log4rs::{
    append::console::{ConsoleAppender, Target},
    config::{Appender, Config, Logger, Root},
    encode::pattern::PatternEncoder,
    Handle,
};
use std::env;
use tpfs_logger_port::{LogError, TpfsLogCfg};

const DEFAULT_PATTERN: &str = "{d(%H:%M:%S)} {h({l})} {M} - {m}{n}";
const PATTERN_ENV_VAR: &str = "XAND_HUMAN_LOG_PATTERN";

pub fn init_with_default_config() -> Result<Handle, LogError> {
    init(&TpfsLogCfg::default())
}

pub fn init(config: &TpfsLogCfg) -> Result<Handle, LogError> {
    log4rs::init_config(tpfscfg_to_log4rs_cfg(config)?).map_err(|e| LogError::InitializationError {
        source: Box::new(e),
    })
}

fn tpfscfg_to_log4rs_cfg(config: &TpfsLogCfg) -> Result<Config, LogError> {
    let console_appender_builder = if TpfsLogCfg::human_readable_mode() {
        let pattern = if let Ok(pat) = env::var(PATTERN_ENV_VAR) {
            pat
        } else {
            DEFAULT_PATTERN.to_string()
        };
        ConsoleAppender::builder().encoder(Box::new(PatternEncoderWithPid::new(
            PatternEncoder::new(&pattern),
        )))
    } else {
        ConsoleAppender::builder().encoder(Box::new(JsonEncoder))
    };

    // Set to write logs to stderr
    let output = console_appender_builder.target(Target::Stderr).build();

    let mut cfg = Config::builder().appender(Appender::builder().build("stdout", Box::new(output)));

    for (target, level) in &config.target_to_level {
        cfg = cfg.logger(Logger::builder().build(target, *level))
    }

    cfg.build(
        Root::builder()
            .appender("stdout")
            .build(config.default_level),
    )
    .map_err(|e| LogError::ConfigurationProblem {
        source: Box::new(e),
    })
}

#[cfg(test)]
mod test {
    use super::*;
    use log::{Level, Record};
    use log4rs::{
        append::Append,
        encode::{Encode, Write},
    };
    use std::{
        error::Error,
        io::Write as StdWrite,
        sync::{Arc, RwLock},
    };

    #[derive(Debug)]
    struct FakeWriter(Vec<u8>);
    impl StdWrite for FakeWriter {
        fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
            self.0.write(buf)
        }

        fn flush(&mut self) -> std::io::Result<()> {
            self.0.flush()
        }
    }
    impl Write for FakeWriter {}

    #[derive(Debug)]
    struct FakeAppender {
        pub last_logged: Arc<RwLock<FakeWriter>>,
        pub encoder: Box<dyn Encode>,
    }

    impl Append for FakeAppender {
        fn append(&self, record: &Record<'_>) -> Result<(), Box<dyn Error + Send + Sync>> {
            let mut writer = self.last_logged.write().expect("Locking logger");
            self.encoder.encode(&mut *writer, record)?;
            writer.flush()?;
            Ok(())
        }

        fn flush(&self) {
            unimplemented!()
        }
    }

    #[test]
    fn test_output_as_expected() {
        // Testing the json encoder is really what we care about here. Testing the overall logger
        // tests not-our-code, and may interfere with other tests.
        let inmem_appender = FakeAppender {
            last_logged: Arc::new(RwLock::new(FakeWriter(vec![]))),
            encoder: Box::new(JsonEncoder),
        };
        inmem_appender
            .append(
                &Record::builder()
                    .level(Level::Info)
                    // The port handles making this arg string json.
                    .args(format_args!("{}", "\"I'm a json string!\""))
                    .target("fake::target")
                    .module_path(Some("fake::path"))
                    .file(Some("filename.rs"))
                    .line(Some(1))
                    .build(),
            )
            .expect("Must be able to construct appender");
        let result = inmem_appender.last_logged.read().expect("Locking logger");
        let result = String::from_utf8(result.0.clone()).expect("Converting string");
        assert_eq!(result,
                   "{\"time\":\"1970-01-01T00:00:00Z\",\"msg\":\"\\\"I\'m a json string!\\\"\",\"module_path\":\"fake::path\",\"file\":\"filename.rs\",\"line\":1,\"level\":\"INFO\",\"target\":\"fake::target\",\"thread\":\"test::test_output_as_expected\"}\n")
    }
}
