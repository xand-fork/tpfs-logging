use derive_more::Constructor;
use log::Record;
use log4rs::encode::{pattern::PatternEncoder, Encode, Write};
use std::env;
use std::error::Error;
use tpfs_logger_port::LOG_PID_ENV_VAR;

// PID is not supported in LOG4RS pattern strings. There is an (old) open MR
// https://github.com/sfackler/log4rs/pull/51
// So we support it separately here with some lame wrapping

#[derive(Debug, Constructor)]
pub struct PatternEncoderWithPid {
    inner_encoder: PatternEncoder,
}

impl Encode for PatternEncoderWithPid {
    fn encode(
        &self,
        w: &mut dyn Write,
        record: &Record<'_>,
    ) -> Result<(), Box<dyn Error + Sync + Send>> {
        self.inner_encoder.encode(w, record)?;
        if env::var(LOG_PID_ENV_VAR).is_ok() {
            let pidstr = format!("(pid: {}) ", std::process::id());
            let _ = w.write(pidstr.as_bytes())?;
        }
        Ok(())
    }
}
