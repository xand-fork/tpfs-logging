#tpfs_logger

This is a [Ports and Adapters](https://softwarecampament.wordpress.com/portsadapters/)-based
implementation of structured logging for Transparent Systems.

This crate represents the `Ports` definition. It's a somewhat imperfect port insofar as it requires
logging event implementations to be `serde` serializable.

The `tpfs_logger_log4rs_adapter` crate represents the `Adapter` portion implemented for the 
`log4rs` crate.

Using this crate consists of defining an enum with our special macro attached. Note that everything
in the enum must be `serde::Serialize`-able.

```rust
#[logging_event]
pub enum LoggingEvent {
    TrustStarted,
    TrustFailedToStart { reason: String },
}
```

Then you can log events like
```rust
info!(LoggingEvent::TrustStarted);
```

Look at you go! Yer' a wizard!

# Adjusting output for humans
Reading JSON is hard for lowly carbon based life. Make things more readable by setting the
env var `XAND_HUMAN_LOGGING` to any value.

You may also want to adjust how things are logged in human readable mode. You can do that by
setting the `XAND_HUMAN_LOG_PATTERN` env var to anything supported as described here:
https://docs.rs/log4rs/0.8.3/log4rs/encode/pattern/index.html

You may also specify that you would like the PID logged, but there is not an option for that
in log4rs patterns (pending https://github.com/sfackler/log4rs/pull/51) -- so it can be manually
enabled by setting `XAND_LOG_PID` to any value (it will be added to the end of the log line).

If you need more detail for a certain module, you can use `XAND_TARGET_LOG_LEVEL` to adjust
the logging level for a specific module. Ex:

`export XAND_TARGET_LOG_LEVEL=xandstrate_client=DEBUG`


## Implementation details you shouldn't have to care about:

The macro turns the above example into something like the following. So, you don't actually get
an enum, but you get something that looks a hell of a lot like an enum in terms of usage. Of course,
you can't pattern match on a `LoggingEvent` since there's no such type in reality -- but you're
not trying to deserialize/read these anyways.

```rust
#[allow(non_snake_case)]
pub mod LoggingEvent {
        use super::*;
        #[derive(serde::Serialize, Debug, Clone)]
        pub struct TrustStarted;

        impl ::tpfs_logger_port::LogKVPairName for TrustStarted {
            fn name(&self) -> &'static str {
                "TrustStarted"
            }
        }
        impl ::tpfs_logger_port::LogKVPairValue for TrustStarted {
            type T = Self;
            fn value(&self) -> Self::T {
                self.clone()
            }
        }

        #[derive(serde::Serialize, Debug, Clone)]
        pub struct TrustFailedToStart {
            pub reason: String,
        }
        impl ::tpfs_logger_port::LogKVPairName for TrustFailedToStart {
            fn name(&self) -> &'static str {
                "TrustFailedToStart"
            }
        }
        impl ::tpfs_logger_port::LogKVPairValue for TrustFailedToStart {
            type T = Self;
            fn value(&self) -> Self::T {
                self.clone()
            }
        }
}
```
