use snafu::Snafu;

#[derive(Debug, Snafu)]
pub enum LogError {
    ConfigurationProblem {
        source: Box<dyn std::error::Error + Sync + Send>,
    },
    InitializationError {
        source: Box<dyn std::error::Error + Sync + Send>,
    },
}
