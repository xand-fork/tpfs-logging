//! This module exposes a clean logging interface for all our code. The `macro`s are really the
//! interface here, and there is an expectation that any adapter will provide an `init` or similar
//! method that registers as the global logger as per the `log` crate.
//!
//! The provided macros provide an alternate version of the standard macros provided by `log`,
//! where they expect to be passed `Loggable` events rather than format strings.
//!
//! So, we heavily lean on the `log` crate as it's the de-facto standard right now in the community

#![warn(clippy::all, clippy::nursery, clippy::pedantic)]
#![allow(bare_trait_objects)]
#![allow(clippy::match_bool)]
// To use the `unsafe` keyword, change to `#![allow(unsafe_code)]` (do not remove); aids auditing.
#![allow(unsafe_code)]
// Safety-critical application lints
#![deny(
    clippy::pedantic,
    clippy::float_cmp_const,
    clippy::integer_arithmetic,
    clippy::option_unwrap_used
)]
// Uncomment before ship to reconcile use of possibly redundant crates, debug remnants, missing license files and more
//#![warn(clippy::cargo, clippy::restriction, missing_docs, warnings)]
//#![deny(warnings)]
#![forbid(unsafe_code)]

// This makes us able to use the procmacro in our unit tests. Neat!
extern crate self as tpfs_logger_port;

mod config;
mod errors;
pub mod macros;

pub use config::TpfsLogCfg;
pub use errors::LogError;
pub use log;
pub use tpfs_log_event_procmacro::logging_event;

use chrono::{DateTime, Utc};
use log::{Level, Log, Record};
use serde::export::fmt::Debug;
use serde::Serialize;
use std::env;

/// If this environment variable is present, adapter implementations are expected to produce
/// human readable output rather than json output.
pub const HUMAN_READABLE_ENV_VAR: &str = "XAND_HUMAN_LOGGING";
/// If this env var is set to any value, log process pids as well
pub const LOG_PID_ENV_VAR: &str = "XAND_LOG_PID";
/// Use this environment variable to adjust the default logging level
pub const LOG_LEVEL_ENV_VAR: &str = "XAND_DEFAULT_LOG_LEVEL";
/// Use this environment variable to adjust logging levels for specific targets. The format of
/// the environment variable is: `path::to::module=level;path::to:another=level`
pub const LOG_TARGET_LEVEL_ENV_VAR: &str = "XAND_TARGET_LOG_LEVEL";

/// The top-level function that the macros call to do logging. It fetches the global logger
/// from the `log` crate with `log::logger()`, and then logs into that.
pub fn log_event<T: Loggable>(
    log_me: T,
    severity: Level,
    target: &str,
    module_path: &str,
    file: &str,
    line: u32,
) {
    let logger = ::log::logger();
    TpfsLog::log(
        &logger,
        severity,
        log_me,
        target,
        SourceInfo {
            module_path: Some(module_path),
            file: Some(file),
            line: Some(line),
        },
    );
}

pub trait Loggable: LogKVPairName + LogKVPairValue + Debug {}
impl<T> Loggable for T where T: LogKVPairName + LogKVPairValue + Debug {}

/// This error type wraps a logger adapter's internal error type to include the event it was
/// trying to log at the time, allowing us to avoid needing to clone an event to alert in the event
/// of logging failure
pub struct LoggingError<E, T: Loggable> {
    pub error: E,
    pub was_trying_to_log: T,
}

/// Primary interface for logging. This crate implements this trait as an extension to the
/// standard`log::Log` trait.
pub trait TpfsLog {
    fn log<E>(&self, level: Level, event: E, target: &str, source_info: SourceInfo<'_>) -> &Self
    where
        E: Loggable;
}

// Note: These traits are broken down so that we can use macros to generate the trivial "name"
// portion, while expecting the user to implement the value portion.

/// The "key" portion of a loggable kv pair
pub trait LogKVPairName {
    fn name(&self) -> &'static str;
}

/// The "value" portion of a loggable kv pair
pub trait LogKVPairValue {
    type T: Serialize + Clone + Debug + 'static;
    // Note: Ideally, you would be able to return `impl serde::Serialize` here, which would allow

    fn value(&self) -> Self::T;
}

impl TpfsLog for &dyn Log {
    fn log<E>(&self, level: Level, event: E, target: &str, source_info: SourceInfo<'_>) -> &Self
    where
        E: Loggable,
    {
        let log_msg = if env::var(HUMAN_READABLE_ENV_VAR).is_ok() {
            format!("{:?}", event)
        } else {
            // Convert the loggable value to json. If it fails, instead log a special message
            // indicating serialization failure, including the debug version of the event.
            let packaged: JsonLoggable<'_, _> = event.into();
            serde_json::to_string(&packaged)
                .unwrap_or_else(|_| format!("{}  {:?}", SERIALIZATION_FAIL_STRING, packaged))
        };
        Log::log(
            *self,
            // Note this *has* to be inline or format_args! won't work
            &Record::builder()
                .level(level)
                .args(format_args!("{}", log_msg))
                .target(target)
                .module_path(source_info.module_path)
                .file(source_info.file)
                .line(source_info.line)
                .build(),
        );
        self
    }
}

/// This effectively our log "schema" -- adapters are expected to log a json-ified instance of
/// this struct per-logging-line.
#[derive(serde_derive::Serialize)]
pub struct JsonEncodedLogMsg<'a> {
    pub time: DateTime<Utc>,
    pub msg: &'a std::fmt::Arguments<'a>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub module_path: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub file: Option<&'a str>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub line: Option<u32>,
    pub level: Level,
    pub target: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub thread: Option<&'a str>,
}

pub struct SourceInfo<'a> {
    pub module_path: Option<&'a str>,
    pub file: Option<&'a str>,
    pub line: Option<u32>,
}

/// Exists to serialize loggables as one json object with the event name and the event value.
#[derive(serde_derive::Serialize, Debug)]
struct JsonLoggable<'a, T: Serialize + Debug> {
    evt: &'a str,
    #[serde(skip_serializing_if = "is_empty")]
    val: T,
}
impl<'a, T> From<T> for JsonLoggable<'a, <T as LogKVPairValue>::T>
where
    T: Loggable,
{
    fn from(loggable: T) -> Self {
        JsonLoggable {
            evt: loggable.name(),
            val: loggable.value(),
        }
    }
}
fn is_empty<T: Serialize>(t: &T) -> bool {
    let stringified = serde_json::to_string(t).unwrap_or_else(|_| "".to_string());
    stringified == "\"\"" || stringified.is_empty()
}

const SERIALIZATION_FAIL_STRING: &str = "Could not serialize event!";
