#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by humans.
    The purpose of this script is to be able to automate the incrementation of a version and
    and preparing it for the ci system along with pushing.

    There shouldn't be any untracked changes in your repository before incrementing. Also this script pushes
    so make sure you're aware of that.

    Arguments
        SEMVER_INCREMENT (Required) = The type of semversion increment that will be made by this script.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

SEMVER_INCREMENT=$1

case $SEMVER_INCREMENT in
     major)
          INCREMENT_FLAG="-M"
          ;;
     minor)
          INCREMENT_FLAG="-m"
          ;;
     patch)
          INCREMENT_FLAG="-p"
          ;;
     *)
          error "ERROR: The SEMVER_INCREMENT passed in doesn't match a known version number type in semversion."
          exit 1
          ;;
esac

git diff-index --quiet HEAD && DIFF=$? || DIFF=$?

if [[ $DIFF -ne 0 ]] ; then
    error "ERROR: You have tracked changes that are uncommitted. You should commit or clear before publishing a version."
    exit 1
fi

function get_version {
    local FILENAME=$1
    echo $(toml get $FILENAME package.version | tr -d '\"')
}

function set_version {
    local FILENAME=$1
    local NEXT_VERSION=$2
    UPDATED_CARGO_TOML="$(toml set $FILENAME package.version $NEXT_VERSION)"
    echo "$UPDATED_CARGO_TOML" > $FILENAME
}

function increment_version {
    local VERSION=$1
    echo "$(./increment_version.sh $INCREMENT_FLAG $VERSION)"
}

CURRENT_VERSION=$(get_version tpfs_logger_port/Cargo.toml)
NEXT_VERSION=$(increment_version $CURRENT_VERSION)

source .env
for directory in "${DIRECTORIES[@]}"
do
    set_version $directory/Cargo.toml $NEXT_VERSION
done

# Looks in dependency order for dependencies in each crate in this workspace that will
# need to have their versions updated in the Cargo.toml files.
for (( i=0; i<${#DIRECTORIES[@]}; i++))
do
    for (( j=0; j<i; j++))
    do
        FILENAME="${DIRECTORIES[$i]}/Cargo.toml"
        DEPENDENCY_FILENAME="${DIRECTORIES[$j]}/Cargo.toml"
        DEPENDENCY_NAME="$(toml get $DEPENDENCY_FILENAME package.name)"
        PACKAGE_DEPENDENCY="$(toml get $FILENAME dependencies.$DEPENDENCY_NAME)"
        if [[ $PACKAGE_DEPENDENCY != "null" ]] ; then
            UPDATED_CARGO_TOML="$(toml set $FILENAME dependencies.$DEPENDENCY_NAME.version \"$NEXT_VERSION\")"
            echo "$UPDATED_CARGO_TOML" > $FILENAME
            UPDATED_CARGO_TOML="$(toml set $FILENAME dependencies.$DEPENDENCY_NAME.registry \"tpfs\")"
            echo "$UPDATED_CARGO_TOML" > $FILENAME
        fi
    done
done

# This should make sure a compile works and increment
# the version in the Cargo.lock file.
cargo make build

git commit -am "v$NEXT_VERSION"
git tag v$NEXT_VERSION
git push
git push --tags
